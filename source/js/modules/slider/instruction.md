## Инструкция по использованию слайдера

**Список файлов**

1. `slider.js` - js файл с исходным кодом
2. `slider.scss` - scss стили для слайдера по умолчанию

**Как это работает**

Для начала вам необходимо сделать разметку для вашего слайдера по следующему шаблону: 
 
 ```html
 <section class="slider">
      <article> Any content</article>
      <article> Any content</article>
      <article> Any content</article>
  </section>
  ```
  
Следующим шагом следует импортировать код и стили слайдера, создать объект слайдер, передав ему селектор ``.slider``, и, если требуется, список параметров.

```javascript
import Slider from './modules/slider/slider';
import './modules/slider/slider.scss'

const sliderParams = {
    autoSwitchTime: 5000
}
const mySlider = new Slider('.slider', sliderParams);
```

Слайдер готов и работает! 

Для более тонкой настройки при создании слайдера можно передать объект с параметрами.
По умолчанию слайдер адаптивный, но можно дать ему фиксированную ширину (в пикселях), передав параметр `slideWidth` 

```javascript
const mySlider = new Slider('.slider', {
    slideWidth: 800
});
```

Поддерживаются следующие параметры:

Приведены значения параметров по умолчанию, `true` - вкл., `false` - откл.
```javascript
const defaultParams = {
    controlButtons: true, //стрелки переключения слайдов, true/false
    controlDots: true, //точки переключения, true/false
    pager: false, // !не реализовано! 
    auto: true, //автоматическое переключение слайдов, true/false
    autoDirection: `right`, // !не реализовано!
    autoSwitchTime: 2000, //время автоматического переключения в ms
    infinityScroll: true, //поддержка "бесконечного" скролла, true/false
    dragSlides: true, //возможность переключения слайдов "перетаскиванем" с помощью мыши, только для десктопа, true/false
    swipeSlides: true, //возможность переключения слайдов касанием, только для обильных устройств и планшетов, true/false 
    slides: 1, //количество контентных блоков в слайде
    activeSlideNumber: 1, //номер активного слайда на момент инициализации сладера
    animationTime: 500, //скорость анимации переключения слайдов
    animationRule: 'ease-in-out', //правило анимации переключения,
    oneMove: false, // возможность переключения контентных блоков в слайде по одному, работает если параметр slides > 1
    marginLeft: 1, //отступ между контентными блоками слева
    marginRight: 1, //отступ между контентными блоками справа
    selectorLeftBtn: ``, //селектор для кнопки листания влево
    selectorRightBtn: ``, //селектор для кнопки листания влево
}
``` 

Также слайдер имеет собственные события и API

События:

```
1. switchright - возникает при смене слайда вправо
2. switchleft - возникает при смене слайда влево
3. sliderload - возникает после инициализации слайдера
4. sliderresize - возникает при изменении размеров слайдера (только для адаптивных слайдеров)
```

Методы API, пример использования:

```javascript
const mySlider = new Slider('.my-slider', sliderParams);
mySlider.API.getAllInfo(); 
```

1. getAllInfo() - получение всей информации о слайдере
2. reloadSlider(newParams) - перезагрузка слайдера с новыми параметрами
3. setActiveSlide(slideNumber) - переключение на слайд с номером slideNumber
4. goToNextSlide() - переключить слайд вправо
5. goToPrevSlide() - переключить слайд влево
6. stopAuto() - остановка автопрокрутки
7. startAuto(time) - начало автопрокрутки с временем переключения time в мс
8. destroySlider() - удаление слайдера из DOM