'use strict';
//TODO 
// 4. try to hide properties
// 5. drag animations
// 6. horizontal direction
// 7. autoDirection to left
// 9. sandbox
// 10. rewrite pager
// 11. update documentation
// 12. edge of next slide
// 13. selectors for dots
export default class Slider {
    constructor(element, userParams = {}) {
        this.slider = document.querySelector(element);
        this.parent = this.slider;
        this.slides = [...this.slider.children];
        this.originalSlides = [...this.slides];
        this.defaultParams = {
            controlButtons: true,
            controlDots: true,
            pager: false,
            auto: true,
            autoSwitchTime: 3000,
            autoDirection: `right`,
            infinityScroll: true,
            dragSlides: false,
            swipeSlides: true,
            slides: 1,
            activeSlideNumber: 1,
            animationTime: 500,
            animationRule: 'ease-in-out',
            oneMove: false,
            marginLeft: 1,
            marginRight: 1,
            selectorLeftBtn: ``,
            selectorRightBtn: ``
        };

        if (userParams.slideWidth) {
            this.slideWidth = userParams.slideWidth;
            this.slider.style.width = `${this.slideWidth}px`;
        } else {
            this.slideWidth = parseFloat(window.getComputedStyle(this.slider).width);
        }
        this.dragOptions = {
            mobile: {
                startEvent: 'touchstart',
                moveEvent: 'touchmove',
                endEvent: 'touchend'
            },
            desktop: {
                startEvent: 'mousedown',
                moveEvent: 'mousemove',
                endEvent: 'mouseup'
            }
        };
        this.defaultParams = { ...this.defaultParams, ...userParams };
        if (this.defaultParams.slides < 2 && this.defaultParams.oneMove) {
            this.defaultParams.oneMove = false;
        }
        this.initSlider();

        this.API = {};

        this.API.reloadSlider = newParams => {
            this.slider = this.slider.parentElement;
            this.slider.innerHTML = ``;
            this.slides = this.originalSlides;
            this.originalSlides.forEach(slide => this.slider.appendChild(slide));
            this.defaultParams = { ...this.defaultParams, ...newParams };
            clearInterval(this.intervalId);
            this.initSlider();
        };

        this.API.getAllInfo = () => ({
            'sliderElement': this.parent,
            'sliderWrapperElement': this.sliderWrapper,
            'sliderWidth': this.sliderWrapper.offsetWidth,
            'dotsAmount': this.dots.children.length,
            'numberActiveSlide': this.indexActiveSlide,
            'initParams': this.defaultParams,
            'slides': this.slides,
            'slidesAmount': this.slides.length,
            'btnPrevElement': this.btnPrev,
            'btnNextElement': this.btnNext
        })


        this.API.setActiveSlide = slideNumber => {
            if (slideNumber > 0 && slideNumber <= this.dots.children.length) {
                this.setActiveSlide(slideNumber, this.defaultParams.animationTime);
                this.changeDotsState(slideNumber + (this.defaultParams.infinityScroll ? 1 : 0));
                this.changeButtonAvailable(this.indexActiveSlide);
            } else {
                const error = new Error(`Parameter should be in the range from 1 to ${this.dots.children.length}`, '', '');
                error.name = `Slider API Error`;
                throw error;
            }
        }

        const publicSlideSwitcher = isNextMethod => {
            let slide = 0;
            if (!this.defaultParams.infinityScroll) {
                slide = this.indexActiveSlide - (isNextMethod ? -1 : 1)
            } else {
                if (isNextMethod) {
                    if (this.indexActiveSlide === (this.dots.children.length - 1)) {
                        slide = 2;
                    } else {
                        slide = this.indexActiveSlide + 1;
                    }
                } else {
                    if (this.indexActiveSlide === 2) {
                        slide = this.dots.children.length - 1;
                    } else {
                        slide = this.indexActiveSlide - 1
                    }
                }
            }
            const emulateEvt = {
                target: {
                    dataset: {
                        slide
                    },
                    classList: {
                        contains: function (param) {
                            if (param === `current`) {
                                return false;
                            }
                            return true;
                        }
                    }
                }
            }

            if (!this.defaultParams.infinityScroll && this.indexActiveSlide === (isNextMethod ? this.dots.children.length : 1)) {
                const error = new Error(`At the ${isNextMethod ? `last` : `first`} slide`, '', '');
                error.name = `Slider API Error`;
                throw error;
            } else {
                this.switchSlide(emulateEvt)
            }
        }

        this.API.goToNextSlide = () => {
            publicSlideSwitcher(true);
        }

        this.API.goToPrevSlide = () => {
            publicSlideSwitcher(false);
        }

        this.API.stopAuto = () => clearInterval(this.intervalId);

        this.API.startAuto = (time = this.defaultParams.autoSwitchTime) => this.refreshAuto(time);

        this.API.destroySlider = () => {
            this.dots.parentElement.removeChild(document.querySelector(`.kam-slider__dots`));
            if (this.defaultParams.controlButtons) {
                if (this.defaultParams.selectorLeftBtn && this.defaultParams.selectorRightBtn) {
                    this.btnNext.classList.remove(`kam-slider__slider-btn--disabled`);
                    this.btnPrev.classList.remove(`kam-slider__slider-btn--disabled`);
                    this.btnNext.classList.dataset = null;
                    this.btnPrev.classList.dataset = null;
                    this.btnNext.removeEventListener('click', window._handlerBtnNext);
                    this.btnPrev.removeEventListener('click', window._handlerBtnPrev);
                } else {
                    this.btnPrev.parentElement.removeChild(document.querySelector(`.kam-slider__slider-btn`));
                    this.btnNext.parentElement.removeChild(document.querySelector(`.kam-slider__slider-btn`));
                }

            }
            this.parent.removeChild(document.querySelector(`.kam-slider__wrapper`));
            this.originalSlides.forEach(slide => {
                slide.classList.remove(`kam-slider__item`);
                slide.style = '';
                this.parent.appendChild(slide)
            });
            this.parent.classList.remove(`kam-slider`);
        }

        const setSliderAsPropertyOfElement = (elementSelector) => {
            document.querySelector(elementSelector).kamSlider = this;
        }

        setSliderAsPropertyOfElement(element);
    }

    initSlider() {
        this.slider.classList.add(`kam-slider`);
        this.createMainWrapper();
        if (this.defaultParams.infinityScroll) {
            this.cloneSlides(this.defaultParams.slides);
            this.setWrapperCondition(0, this.sliderWrapper.offsetWidth);
        }
        if (this.defaultParams.controlButtons) {
            if (this.defaultParams.selectorLeftBtn && this.defaultParams.selectorRightBtn) {
                this.btnNext = document.querySelector(this.defaultParams.selectorRightBtn);
                this.btnPrev = document.querySelector(this.defaultParams.selectorLeftBtn);
                this.btnNext.dataset.slideBtn = `right`;
                this.btnPrev.dataset.slideBtn = `left`;
                window._handlerBtnNext = this.btnSwitcher.bind(this);
                window._handlerBtnPrev = this.btnSwitcher.bind(this);
                this.btnNext.addEventListener('click', window._handlerBtnNext);
                this.btnPrev.addEventListener('click', window._handlerBtnPrev);
            } else {
                this.btnPrev = this.createButton('left');
                this.btnNext = this.createButton('right');
            }
            if (!this.defaultParams.infinityScroll) {
                if (this.defaultParams.activeSlideNumber === 1) {
                    this.btnPrev.classList.add(`kam-slider__slider-btn--disabled`);
                    this.btnDisabled = 'left';
                }
                if (this.defaultParams.activeSlideNumber === this.computeDots()) {
                    this.btnNext.classList.add(`kam-slider__slider-btn--disabled`);
                    this.btnDisabled = 'right';
                }
            }
        }
        if (this.defaultParams.pager) {
            this.loadPager();
            this.defaultParams.infinityScroll ? this.infinityPager = true : this.infinityPager = false;
        }
        this.loadControlDots();
        if (this.defaultParams.auto) {
            this.intervalId = setInterval(this.autoSwitchSlide.bind(this), this.defaultParams.autoSwitchTime);
        }

        if (this.defaultParams.activeSlideNumber > 1 && this.defaultParams.activeSlideNumber <= this.computeDots() - (this.defaultParams.infinityScroll ? 2 : 0)) {
            this.setActiveSlide(this.defaultParams.activeSlideNumber, 0);
        }
        this.setDimensionsForSlides();
        this.setListenerToWindowResize();

        const event = new CustomEvent(`sliderload`, {
            detail: {
                activeSlideNumber: this.indexActiveSlide
            }
        });
        setTimeout(() => this.parent.dispatchEvent(event), 100);
    }

    setDimensionsForSlides() {
        this.slides.forEach(slide => {
            slide.style.width = `${(100 / this.defaultParams.slides) - (this.defaultParams.marginLeft + this.defaultParams.marginRight)}%`;
            slide.style.marginLeft = `${this.defaultParams.marginLeft}%`;
            slide.style.marginRight = `${this.defaultParams.marginRight}%`;
        });
    }

    setActiveSlide(slideNumber, setTime) {
        const offsetFirst = (this.sliderWrapper.offsetWidth / (this.defaultParams.oneMove ? this.defaultParams.slides : 1));
        let offsetSecond = 0;
        if (this.defaultParams.infinityScroll) {
            offsetSecond = slideNumber + (this.defaultParams.oneMove ? this.defaultParams.slides - 1 : 0);
        } else {
            offsetSecond = slideNumber - 1;
        }
        const offset = offsetFirst * offsetSecond;
        this.setWrapperCondition(setTime, offset);
    }

    isMobileDevice() {
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent);
    }

    getCurrentSlidePosition() {
        return parseFloat(this.sliderWrapper.style.transform.match(/(?=.)([+-]?(?=[\d\.])(\d*)(\.(\d+))?)/ig)[0]);
    }


    sliderDragStart(evt) {
        clearInterval(this.intervalId);
        this.sliderWrapper.style.transition = 'none';
        const dragPosition = this.getCurrentSlidePosition();
        const mousePosition = evt.touches ? evt.touches[0].clientX : evt.clientX;
        const onDragMove = evt => {
            const clientXMove = evt.touches ? evt.touches[0].clientX : evt.clientX;
            const slideShift = clientXMove - mousePosition;
            const finalTranslate = slideShift + dragPosition;
            if (!this.defaultParams.infinityScroll && (finalTranslate < 0 && finalTranslate > -this.sliderWrapper.offsetWidth * (this.slides.length - 1) && finalTranslate > (-this.sliderWrapper.offsetWidth / this.defaultParams.slides) * (this.slides.length - this.defaultParams.slides))) {
                this.sliderWrapper.style.transform = `translateX(${finalTranslate}px)`;
            } else if (this.defaultParams.infinityScroll) {
                this.sliderWrapper.style.transform = `translateX(${finalTranslate}px)`;
            }
            //TODO drag animation for each slide
            // console.log(document.querySelectorAll(`.kam-slider__item`)[2]);
            // document.querySelectorAll(`.kam-slider__item`)[2].style.transform = `scale(${1 - (-slideShift / (this.slideWidth / this.defaultParams.slides))})`;
        };
        const onDragEnd = evt => {
            this.slider.style.transition = `transform ${this.defaultParams.animationTime}ms ${this.defaultParams.animationRule}`;
            const clientXEnd = evt.changedTouches ? evt.changedTouches[0].clientX : evt.clientX;
            const dragDirection = mousePosition - clientXEnd > 0 ? 1 : -1;
            const dragPositionEnd = this.getCurrentSlidePosition();
            const shiftAfterDrag = (-dragPosition + dragPositionEnd) * dragDirection;
            if (shiftAfterDrag > (-this.sliderWrapper.offsetWidth / this.defaultParams.slides) * 0.2) {
                this.sliderWrapper.style.transform = `translateX(${dragPosition}px)`;
            } else {
                const prevSlide = this.indexActiveSlide;
                let notLastSlide = true;
                if (this.defaultParams.infinityScroll && !this.defaultParams.oneMove && this.defaultParams.slides < 2) {
                    this.sliderWrapper.style.transform = `translateX(${(dragPosition - (dragDirection * (this.sliderWrapper.offsetWidth)))}px)`;
                    notLastSlide = dragDirection === 1 ?
                        this.checkBoundariesSlides(this.slides.length - 2, prevSlide - 1, 2) :
                        this.checkBoundariesSlides(1, prevSlide - 1, this.slides.length - 1);
                } else if (this.defaultParams.infinityScroll && this.defaultParams.oneMove) {
                    this.sliderWrapper.style.transform = `translateX(${(dragPosition - (dragDirection * (this.sliderWrapper.offsetWidth / this.defaultParams.slides)))}px)`;
                    notLastSlide = dragDirection === 1 ?
                        this.checkBoundariesSlides(this.slides.length - (this.defaultParams.slides * 2), prevSlide - 1, 2) :
                        this.checkBoundariesSlides(1, prevSlide - 1, this.slides.length - (this.defaultParams.slides * 2) + 1);
                } else if (this.defaultParams.infinityScroll && !this.defaultParams.oneMove && this.defaultParams.slides > 1) {
                    this.sliderWrapper.style.transform = `translateX(${(dragPosition - (dragDirection * (this.sliderWrapper.offsetWidth)))}px)`;
                    notLastSlide = dragDirection === 1 ?
                        this.checkBoundariesSlides(((this.slides.length - (this.defaultParams.slides * 2)) / this.defaultParams.slides), prevSlide - 1, 2) :
                        this.checkBoundariesSlides(1, prevSlide - 1, ((this.slides.length - (this.defaultParams.slides * 2)) / this.defaultParams.slides) + 1);
                } else {
                    this.defaultParams.oneMove ? this.sliderWrapper.style.transform = `translateX(${dragPosition - (dragDirection * (this.sliderWrapper.offsetWidth / this.defaultParams.slides))}px)` : this.sliderWrapper.style.transform = `translateX(${dragPosition - (dragDirection * (this.sliderWrapper.offsetWidth))}px)`;
                }

                // this.defaultParams.pager ? Slider.refreshPager(this.infinityPager) : '';

                const nextSlide = dragDirection === -1 ? prevSlide - 1 : prevSlide + 1;
                if (nextSlide !== this.slides.length + 1 && nextSlide !== 0) {
                    if (notLastSlide) {
                        this.changeDotsState(nextSlide);
                    }
                    this.changeButtonAvailable(this.indexActiveSlide);
                    if (dragDirection === 1) {
                        this.dispatchSwitchEvent(`right`, prevSlide);
                    }

                    if (dragDirection === -1) {
                        this.dispatchSwitchEvent(`left`, prevSlide);
                    }
                }
            }
            if (this.defaultParams.auto) {
                this.refreshAuto(this.defaultParams.autoSwitchTime);
            }
            document.removeEventListener(evt.changedTouches ? this.dragOptions.mobile.moveEvent : this.dragOptions.desktop.moveEvent, onDragMove);
            document.removeEventListener(evt.changedTouches ? this.dragOptions.mobile.endEvent : this.dragOptions.desktop.endEvent, onDragEnd);
        };
        document.addEventListener(evt.touches ? this.dragOptions.mobile.moveEvent : this.dragOptions.desktop.moveEvent, onDragMove);
        document.addEventListener(evt.touches ? this.dragOptions.mobile.endEvent : this.dragOptions.desktop.endEvent, onDragEnd);
    }

    createMainWrapper() {
        this.sliderWrapper = document.createElement('div');
        this.sliderWrapper.classList.add(`kam-slider__wrapper`);
        this.slides.forEach(slide => {
            slide.classList.add(`kam-slider__item`);
            this.sliderWrapper.appendChild(slide)
        });
        this.slider.appendChild(this.sliderWrapper);
        this.sliderWrapper.style.transform = 'translateX(0px)';
        if (this.defaultParams.dragSlides) {
            this.sliderWrapper.addEventListener(this.dragOptions.desktop.startEvent, this.sliderDragStart.bind(this));
        }
        if (this.defaultParams.swipeSlides && this.isMobileDevice) {
            this.sliderWrapper.addEventListener(this.dragOptions.mobile.startEvent, this.sliderDragStart.bind(this));
        }
    }

    pasteCloneNodes(slideToClone) {
        slideToClone.nodes.forEach(node => {
            this.sliderWrapper.insertAdjacentElement(slideToClone.place, node.cloneNode(true));
        });
    }

    cloneSlides(slidesPerSlide) {
        const slidesToClone = [
            { place: 'beforeend', nodes: [] },
            { place: 'afterbegin', nodes: [] }
        ];

        for (let i = 0; i < slidesPerSlide; i++) {
            slidesToClone[0].nodes.push(this.slides[i]);
            slidesToClone[1].nodes.push(this.slides[this.slides.length - 1 - i]);
        }

        slidesToClone.forEach(this.pasteCloneNodes.bind(this));
        this.slides = [].slice.call(this.sliderWrapper.children);
    }

    createButton(direction) {
        const sliderButton = document.createElement('div');
        sliderButton.classList.add(`kam-slider__slider-btn`, `kam-slider__slider-btn--${direction}`);
        sliderButton.dataset.slideBtn = direction;
        this.slider.appendChild(sliderButton);
        sliderButton.addEventListener('click', this.btnSwitcher.bind(this));
        return sliderButton;
    }

    computeDots() {
        let dotsAmount = 0;
        if (this.defaultParams.infinityScroll && this.defaultParams.oneMove) {
            dotsAmount = this.slides.length - (this.defaultParams.slides * 2) + 2;
        } else if (this.defaultParams.oneMove) {
            dotsAmount = this.slides.length - this.defaultParams.slides + 1;
        } else if (this.defaultParams.infinityScroll && this.defaultParams.slides < 2) {
            dotsAmount = this.slides.length;
        } else if ((this.defaultParams.infinityScroll && this.defaultParams.slides > 1) || (!this.defaultParams.infinityScroll && !this.defaultParams.oneMove)) {
            dotsAmount = Math.ceil(this.slides.length / this.defaultParams.slides);
        }
        return dotsAmount;
    }

    loadControlDots() {
        this.dots = document.createElement('ul');
        this.dots.classList.add(`kam-slider__dots`);
        if (!this.defaultParams.controlDots) {
            this.dots.style.display = 'none';
        }
        const dotsAmount = this.computeDots();
        for (let i = 0; i < dotsAmount; i++) {
            const dot = document.createElement('li');
            if (i === 0 && !this.defaultParams.infinityScroll && this.defaultParams.activeSlideNumber === 1) {
                dot.classList.add('current');
            } else if (i === 1 && this.defaultParams.infinityScroll && this.defaultParams.activeSlideNumber === 1) {
                dot.classList.add('current');
            }
            dot.classList.add(`kam-slider__control-dot`);
            dot.dataset.slide = i + 1;
            this.dots.appendChild(dot);
        }
        this.slider.appendChild(this.dots);
        this.slider = this.sliderWrapper;
        this.dots.addEventListener('click', this.switchSlide.bind(this));
        if (this.defaultParams.infinityScroll) {
            this.dots.children[0].style.display = 'none';
            this.dots.children[this.dots.children.length - 1].style.display = 'none';
        }
        if (this.defaultParams.activeSlideNumber > 1 && this.defaultParams.activeSlideNumber <= dotsAmount - (this.defaultParams.infinityScroll ? 2 : 0)) {
            this.dots.children[this.defaultParams.activeSlideNumber - (this.defaultParams.infinityScroll ? 0 : 1)].classList.add(`current`);
        } else {
            this.dots.children[this.defaultParams.infinityScroll ? 1 : 0].classList.add(`current`);
        }
        this.currentDot = this.dots.querySelector(`.current`);
        this.indexActiveSlide = parseInt(this.currentDot.dataset.slide, 10) /*+ (this.defaultParams.infinityScroll ? 1 : 0)*/;
    }

    loadPager() {
        const createPagerElement = params => {
            const pagerElement = document.createElement('span');
            pagerElement.classList.add(params['className']);
            pagerElement.textContent = params['textContent'];
            let parentElement = this.pager;
            if (params['parentElement']) {
                parentElement = this.slider;
                this.pager = pagerElement;
            }
            parentElement.appendChild(pagerElement);
        };
        const pagerElements = [
            { parentElement: true, className: `kam-slider__pager`, textContent: '' },
            { className: `kam-slider__pager-current`, textContent: '1', },
            { className: `kam-slider__pager-delimiter`, textContent: '|', },
            {
                className: `kam-slider__pager-current`,
                textContent: this.defaultParams.infinityScroll ? this.slides.length - 2 : this.slides.length
            }
        ];
        pagerElements.forEach(createPagerElement);
    }

    changeClonedSlide(newSlide) {
        let transtitionValue = 0;
        if (this.defaultParams.oneMove) {
            if (newSlide === 2) {
                transtitionValue = this.sliderWrapper.offsetWidth;
            } else if ((newSlide === this.dots.children.length - 1) && this.defaultParams.oneMove) {
                transtitionValue = (this.dots.children.length - 3 + this.defaultParams.slides) * (this.sliderWrapper.offsetWidth / this.defaultParams.slides);
            }
        }
        else {
            if (newSlide === 2) {
                transtitionValue = this.sliderWrapper.offsetWidth;
            } else {
                transtitionValue = this.sliderWrapper.offsetWidth * (this.dots.children.length - 2);
            }

        }
        return setTimeout(() => this.setWrapperCondition(0, transtitionValue), this.defaultParams.animationTime);
    }


    checkBoundariesSlides(valueToCompare, prevSlide, newSlide) {
        if (prevSlide === valueToCompare) {
            prevSlide = newSlide;
            this.changeDotsState(prevSlide);
            this.changeClonedSlide(newSlide);
            return false;
        }
        return true;
    }

    btnSwitcher(evt) {
        this.slider.offsetParent.style.pointerEvents = 'none';
        const btnType = evt.target.dataset.slideBtn;
        const prevSlide = this.indexActiveSlide;
        let notLastSlide = true;
        if (this.defaultParams.infinityScroll && !this.defaultParams.oneMove && this.defaultParams.slides < 2) {
            notLastSlide = btnType === 'right' ?
                this.checkBoundariesSlides(this.slides.length - 2, prevSlide - 1, 2) :
                this.checkBoundariesSlides(1, prevSlide - 1, this.slides.length - 1);
        } else if (this.defaultParams.infinityScroll && this.defaultParams.oneMove) {
            notLastSlide = btnType === 'right' ?
                this.checkBoundariesSlides(this.slides.length - (this.defaultParams.slides * 2), prevSlide - 1, 2) :
                this.checkBoundariesSlides(1, prevSlide - 1, this.slides.length - (this.defaultParams.slides * 2) + 1);
        } else if (this.defaultParams.infinityScroll && !this.defaultParams.oneMove && this.defaultParams.slides > 1) {
            notLastSlide = btnType === 'right' ?
                this.checkBoundariesSlides(((this.slides.length - (this.defaultParams.slides * 2)) / this.defaultParams.slides), prevSlide - 1, 2) :
                this.checkBoundariesSlides(1, prevSlide - 1, ((this.slides.length - (this.defaultParams.slides * 2)) / this.defaultParams.slides) + 1);
        }

        const nextSlide = btnType === 'left' ? prevSlide - 1 : prevSlide + 1;
        if (nextSlide !== this.slides.length + 1 && nextSlide !== 0) {
            if (notLastSlide) {
                this.changeDotsState(nextSlide);
                this.changeButtonAvailable(this.indexActiveSlide);
            }
            this.defaultParams.oneMove ? this.showSlide(this.sliderWrapper.offsetWidth / this.defaultParams.slides, btnType) : this.showSlide(this.sliderWrapper.offsetWidth, btnType);
            setTimeout(() => {
                this.slider.offsetParent.style.pointerEvents = 'auto';
            }, this.defaultParams.animationTime);
        }

        if (this.defaultParams.auto) {
            this.refreshAuto(this.defaultParams.autoSwitchTime);
        }
        if (this.defaultParams.pager) {
            Slider.refreshPager(this.infinityPager);
        }

        if (btnType === 'right') {
            this.dispatchSwitchEvent(btnType, prevSlide);
        }

        if (btnType === 'left') {
            this.dispatchSwitchEvent(btnType, prevSlide);
        }
    }

    dispatchSwitchEvent(direction, prevSlide) {
        const event = new CustomEvent(`switch${direction}`, {
            detail: {
                slideIndexBefore: this.defaultParams.infinityScroll ? prevSlide - 1 : prevSlide,
                slideIndexAfter: this.defaultParams.infinityScroll ? this.indexActiveSlide - 1 : this.indexActiveSlide
            }
        });
        this.slider.offsetParent.dispatchEvent(event);
    }

    showSlide(offset, direction) {
        this.slider.style.transition = `transform ${this.defaultParams.animationTime}ms ${this.defaultParams.animationRule}`;
        const currentTranslate = this.getCurrentSlidePosition();
        if (direction === 'right') {
            this.slider.style.transform = `translateX(${currentTranslate - offset}px)`;
        } else {
            this.slider.style.transform = `translateX(${currentTranslate + offset}px)`;
        }
    }

    changeButtonAvailable(slideNumberActive) {
        this.btnPrev.classList.remove(`kam-slider__slider-btn--disabled`);
        this.btnNext.classList.remove(`kam-slider__slider-btn--disabled`);
        if (slideNumberActive === 1) {
            this.btnPrev.classList.add(`kam-slider__slider-btn--disabled`);
        }
        if (slideNumberActive === this.dots.children.length) {
            this.btnNext.classList.add(`kam-slider__slider-btn--disabled`);
        }
    }

    switchSlide(evt) {
        const slideNumberPrev = this.indexActiveSlide;
        const slideNumberActive = parseInt(evt.target.dataset.slide, 10);
        const difference = slideNumberPrev - slideNumberActive;
        let direction = ``;
        if (evt.target.classList.contains(`kam-slider__control-dot`) && !evt.target.classList.contains('current')) {
            if (this.defaultParams.infinityScroll && Math.abs(difference) === this.dots.children.length - 3 && !this.defaultParams.oneMove) {
                direction = difference < 0 ? 'left' : 'right';
                this.showSlide(this.sliderWrapper.offsetWidth, direction);
                this.changeClonedSlide(difference > 0 ? 2 : this.slides.length - (this.defaultParams.slides * 2));
            } else if (this.defaultParams.infinityScroll && Math.abs(difference) === this.dots.children.length - 3 && this.defaultParams.oneMove) {
                direction = difference < 0 ? 'left' : 'right';
                this.showSlide((this.sliderWrapper.offsetWidth / this.defaultParams.slides), direction);
                this.changeClonedSlide(difference > 0 ? 2 : this.slides.length - (this.defaultParams.slides * 2) + 1);
            } else {
                direction = difference < 0 ? 'right' : 'left'
                this.showSlide((this.sliderWrapper.offsetWidth / (this.defaultParams.oneMove ? this.defaultParams.slides : 1)) * Math.abs(difference), direction);
                this.changeButtonAvailable(slideNumberActive);
            }
            this.changeDotsState(slideNumberActive);
            this.changeButtonAvailable(this.indexActiveSlide);
        }

        if (direction === 'right') {
            this.dispatchSwitchEvent(direction, slideNumberPrev);
        }

        if (direction === 'left') {
            this.dispatchSwitchEvent(direction, slideNumberPrev);
        }
        if (this.defaultParams.auto) {
            this.refreshAuto(this.defaultParams.autoSwitchTime);
        }
        if (this.defaultParams.pager) {
            Slider.refreshPager(this.infinityPager);
        }
    }

    autoSwitchSlide() {
        let currentDot = this.indexActiveSlide;
        const dots = [...this.dots.children];
        let lastDot = parseInt(dots[dots.length - 1].dataset.slide);
        if (this.defaultParams.infinityScroll) {
            lastDot -= 1;
        }
        let previousDot = currentDot;
        let direction = 'right';
        if (currentDot === lastDot) {
            previousDot = lastDot;
            currentDot = 1;
            direction = 'left';
        } else {
            currentDot++;
        }

        if (currentDot === 1 && this.defaultParams.infinityScroll) {
            currentDot = 2;
            direction = 'right';
            this.showSlide(-this.sliderWrapper.offsetWidth / (this.defaultParams.oneMove ? this.defaultParams.slides : 1), 'left');
            setTimeout(() => {
                this.sliderWrapper.style.transition = `transform 0s ${this.defaultParams.animationRule} 0s`;
                this.sliderWrapper.style.transform = `translateX(${-this.sliderWrapper.offsetWidth}px)`;
            }, this.defaultParams.animationTime);
        } else if (this.defaultParams.oneMove) {
            this.showSlide((this.sliderWrapper.offsetWidth / this.defaultParams.slides) * Math.abs(previousDot - currentDot), direction);
        } else if (!this.defaultParams.oneMove) {
            this.showSlide((this.sliderWrapper.offsetWidth) * Math.abs(previousDot - currentDot), direction);
        }
        this.changeDotsState(currentDot);
        this.changeButtonAvailable(this.indexActiveSlide);

        if (direction === 'right') {
            this.dispatchSwitchEvent(direction, previousDot);
        }

        if (direction === 'left') {
            this.dispatchSwitchEvent(direction, previousDot);
        }
        // if (this.defaultParams.pager) {
        //     Slider.refreshPager(this.infinityPager);
        // }
    }

    refreshAuto(time) {
        clearTimeout(this.intervalId);
        this.intervalId = setInterval(this.autoSwitchSlide.bind(this), time);
    }

    setWrapperCondition(transitionTransformTime, translateX) {
        this.sliderWrapper.style.transition = `transform ${transitionTransformTime}ms ${this.defaultParams.animationRule} 0s`;
        this.sliderWrapper.style.transform = `translateX(-${translateX}px)`;
    }

    setListenerToWindowResize() {
        window.addEventListener('resize', () => {
            this.slideWidth = this.sliderWrapper.offsetWidth;
            this.setWrapperCondition(0, 0);
            const commonCompute = (-1 * (this.sliderWrapper.offsetWidth / (this.defaultParams.oneMove ? this.defaultParams.slides : 1)));
            if (this.defaultParams.infinityScroll) {
                this.slider.style.transform = `translateX(${commonCompute * (this.indexActiveSlide - 1 + (this.defaultParams.oneMove ? this.defaultParams.slides - 1 : 0))}px)`;
            } else {
                this.slider.style.transform = `translateX(${commonCompute * (this.indexActiveSlide - 1)}px)`;
            }
            const event = new CustomEvent(`sliderresize`);
            this.slider.offsetParent.dispatchEvent(event);
        });
    }

    static refreshPager(infinityPager) {
        const infinityPagerCurrent = infinityPager ? 1 : 0;
        // document.querySelector(`.${Slider.element}__pager-current`).textContent = document.querySelector(`.${Slider.element}__control-dot.current`).getAttribute('data-slide') - infinityPagerCurrent;
    }

    clearCurrentDot() {
        [...this.dots.children].forEach(element => element.classList.remove('current'));
    };

    setCurrentDot(dotNumber) {
        this.dots.children[dotNumber - 1].classList.add('current');
        this.indexActiveSlide = parseInt(this.dots.querySelector(`.current`).dataset.slide);
    }

    changeDotsState(newCurrentDot) {
        this.clearCurrentDot();
        this.setCurrentDot(newCurrentDot);
    }
}