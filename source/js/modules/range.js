'use strict';
import debounce from './debounce';

export default class Draggable {
    constructor(element, params = {minValue: -10, maxValue: 10, dragInput: true, dragFollow: true}) {
        this.diapason = {
            minValue: params.minValue,
            maxValue: params.maxValue
        };
        this.dragContainer = document.querySelector(element);
        this.dragInput = params.dragInput;
        this.dragFollow = params.dragFollow;
        this.init();
    }

    init() {
        const range = document.createElement('div');
        range.classList.add('drag-range');
        this.dragContainer.appendChild(range);

        if (this.dragFollow) {
            const rangeFollow = document.createElement('div');
            rangeFollow.classList.add('range-follow');
            range.appendChild(rangeFollow);
            this.rangeFollow = rangeFollow;
        }


        const drag = document.createElement('div');
        drag.classList.add('draggable');
        range.appendChild(drag);
        if (this.dragInput) {
            const dragValue = document.createElement('input');
            dragValue.type = 'text';
            dragValue.classList.add('drag-value');
            this.dragContainer.appendChild(dragValue);
            this.dragValue = dragValue;
            this.dragValue.value = this.diapason.minValue;
        }


        this.drag = drag;
        this.range = range;
        this.drag.addEventListener('mousedown', this.onDragStart.bind(this));
        this.range.addEventListener('click', this.setDrag.bind(this));
        this.maxShift = this.range.offsetWidth - this.drag.offsetWidth;
        window.lastTimeout = 0;
        window.addEventListener('resize', () => {
            debounce(this.reloadRange.bind(this));
        });
    }

    onDragStart(e) {

        this.drag.style.transition = null;
        if (this.rangeFollow) {
            this.rangeFollow.style.transition = null;
        }
        const Coordinate = function (x) {
            this.x = x;
        };
        const initCoord = new Coordinate(0);
        if (this.drag.style.left) {
            initCoord.x = parseInt(this.drag.style.left, 10);
        }
        this.drag.style.left = `${initCoord.x}px`;

        const mouseOffset = new Coordinate(e.clientX);
        const onDragMove = (e) => {
            let shiftCoord = new Coordinate(e.clientX - mouseOffset.x);
            shiftCoord.x += initCoord.x;
            if (shiftCoord.x < 0) {
                shiftCoord.x = 0;
            }
            if (shiftCoord.x > this.maxShift) {
                shiftCoord.x = this.maxShift;
            }
            this.drag.style.left = `${shiftCoord.x}px`;
            if (this.rangeFollow) {
                this.rangeFollow.style.width = `${shiftCoord.x + 1 }px`;
            }
            if (this.dragInput) {
                this.dragValue.value = Draggable.computeDragValue(shiftCoord.x, this.maxShift, this.diapason.minValue, this.diapason.maxValue);
            }
        };
        const onDragEnd = (e) => {
            document.removeEventListener('mousemove', onDragMove);
            document.removeEventListener('mouseup', onDragEnd);
        };
        document.addEventListener('mousemove', onDragMove);
        document.addEventListener('mouseup', onDragEnd);
    }

    setDrag(e) {
        if (e.target.classList.contains('drag-range') || e.target.classList.contains('range-follow')) {
            this.drag.style.transition = 'left 400ms';
            let totalOffset = e.offsetX - this.drag.offsetWidth / 2;
            if (totalOffset < 0) {
                totalOffset = 0;
            }
            if (this.rangeFollow) {
                this.rangeFollow.style.transition = 'width 400ms';
                this.rangeFollow.style.width = `${totalOffset + 1}px`;
            }

            this.drag.style.left = `${totalOffset}px`;

            if (this.dragInput) {
                this.dragValue.value = Draggable.computeDragValue(parseInt(this.drag.style.left, 10), this.maxShift, this.diapason.minValue, this.diapason.maxValue);
            }
        }
    }

    static computeDragValue(shift, maxShift, minValue, maxValue) {
        return Math.floor(shift / maxShift * (-minValue + maxValue) + minValue);
    }

    reloadRange() {
        this.dragContainer.innerHTML = "";
        this.init();
        console.log('call');
    }
}