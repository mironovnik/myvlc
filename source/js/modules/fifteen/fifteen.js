export default class Fifteen {
    constructor(){
        this.playState = [
            1, 2, 3, 4,
            5, 6, 7, 8,
            9, 10, 11, 12,
            13, 14, 15, 0
        ];
        this.initGame();
    }

    static shufflePlayground = array => array.sort(() => Math.random() - 0.5);

    createPlayPlate = plateNumber => {
        const playPlate = document.createElement(`div`);
        playPlate.classList.add(`game-board__playing-plate`);
        playPlate.textContent = plateNumber;
        this.playground.insertAdjacentElement('beforeend',playPlate);
    };



    initGame() {
        this.playState = Fifteen.shufflePlayground(this.playState);
        this.playState.forEach(this.createPlayPlate);
        this.playPlates = document.querySelectorAll('.game-board__playing-plate');
        this.playPlates.forEach(playPlate => {
            playPlate.addEventListener('mousedown', ()=> {
                console.log('mousedown');
            })
        })
    }

}