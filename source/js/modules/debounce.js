'use strict';

const debounce = function (callback) {
        if (window.lastTimeout) {
            clearTimeout(window.lastTimeout);
        }
        window.lastTimeout = setTimeout(function() {
            callback();
        }, 300);
};

export default debounce;