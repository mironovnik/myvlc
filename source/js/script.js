import '../sass/style.scss';
import Draggable from './modules/range';
import Slider from './modules/slider/slider';
import Fifteen from './modules/fifteen/fifteen'
import './modules/fifteen/fifteen';

if (document.querySelector('.my-slider')) {
    const sliderParams = {
        auto: false,
        dragSlides: true,
        autoSwitchTime: 1000,
        activeSlideNumber: 1,
        marginLeft: 2,
        marginRight: 2,
        slides: 1,
        oneMove: false,
        infinityScroll: false,
        animationTime: 500,
        selectorLeftBtn: `.fake-btn.left`,
        selectorRightBtn: `.fake-btn.right`
        // slideWidth: 800
    }
    const mySlider = new Slider('.my-slider', sliderParams);
    window.mySlider = mySlider; // to test
    // console.log(`mySlider===>`, mySlider);
}

// if (document.querySelector('.my-slider1')) {
//     const sliderParams = {
//         auto: true,
//         marginLeft: 2,
//         marginRight: 2,
//         autoSwitchTime: 1500,
//         slides: 2,
//         infinityScroll: false,
//     }
//     const mySlider = new Slider('.my-slider1', sliderParams);
// }
if (document.querySelector('.drag-plugin__drag-container')){
    const drag = new Draggable('.drag-plugin__drag-container');
}
if (document.querySelector(`.game-board__playing-area`)) {
    const fifteen = new Fifteen(`.game-board__playing-area`);
}

// const fifteen = new Fifteen();


const importAll = function(context) {
    let files = {};
    context.keys().map((item) => {
        files[item.replace('./', '')] = context(item);
    });
    return files;
};
const images = importAll(require.context('../img/', false, /\.(png|jpe?g|svg)$/));
const fonts =  importAll(require.context('../fonts/', false, /\.(eot|svg|ttf|woff|woff2)$/));

