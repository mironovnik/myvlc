const webpack = require('webpack');
const path = require('path');
const argv = require('yargs').argv;
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const fs = require('fs');
const isDevelopment = argv.mode === 'development';
const isProduction = !isDevelopment;
const pages =
    fs
        .readdirSync(path.resolve(__dirname, 'source'))
        .filter(fileName => fileName.endsWith('.html'));
module.exports = {
    entry: {main: './source/js/script.js'},
    output: {
        path: path.resolve(process.cwd(), 'build'),
        filename: 'js/bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    plugins: ['transform-class-properties', 'transform-object-rest-spread']
                }
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                ]
            },
            {
                test: /\.(gif|png|jpe?g|svg|webp)$/i,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: 'img/[name].[ext]'
                    }
                }, {
                    loader: 'image-webpack-loader',
                    options: {
                        mozjpeg: {
                            progressive: true,
                            quality: 70
                        }
                    }
                },
                ],
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/i,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: 'fonts/[name].[ext]'
                    }
                }]
            }
        ]
    },
    plugins: [
        new webpack.ProgressPlugin(),
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: "[name].css",
        }),
        ...pages.map(page => new HtmlWebpackPlugin({
            template: `./source/${page}`,
            filename: page
        }))
    ]

};